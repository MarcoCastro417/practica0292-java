package com.example.clima92_java;

import androidx.appcompat.app.AppCompatActivity;
import com.example.clima92_java.R;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}